#pragma once
#include <utility>
#include "defaults.hpp"
#include "cell.hpp"
#include <exception>

namespace sudoku {

	struct Coord : std::pair<int, int> {
		using std::pair<int, int>::pair;
		int& row() {
			return first;
		}
		int row() const {
			return first;
		}
		int& col() {
			return second;
		}
		int col() const {
			return second;
		}
	};

	class InvalidCoord : virtual public std::exception {};

	template <int ORDER = CANONICAL_ORDER,
			  typename C = Cell<ORDER>>
	class Grid {
	public:
		static constexpr int order{ORDER};
		static constexpr int size{ORDER*ORDER};

	private:
		C cells_[size][size];

	public:
		C& operator[](const Coord& coord) {
			if (!isValidCoord(coord)) {
				throw InvalidCoord{};
			}
			return cells_[coord.row()-1][coord.col()-1];
		}

	public:
		static bool isValidCoord(Coord coord) {
			return coord.row() >= 1 && coord.row() <= size
				&& coord.col() >= 1 && coord.col() <= size;
		}
		static int rowOf(Coord coord) {
			return isValidCoord(coord) ? coord.row() : 0;
		}
		static int colOf(Coord coord) {
			return isValidCoord(coord) ? coord.col() : 0;
		}
		static int boxOf(Coord coord) {
			if (!isValidCoord(coord)) {
				return 0;
			}
			int boxBase = ((coord.row()-1) / order) * order;
			int boxOffset = (coord.col()-1) / order + 1;
			return boxBase + boxOffset;
		}
	};

	template <int ORDER, typename C>
	constexpr int Grid<ORDER, C>::order;

	template <int ORDER, typename C>
	constexpr int Grid<ORDER, C>::size;

}
