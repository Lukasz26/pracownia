#include "catch.hpp"
#include "topology.hpp"

TEST_CASE("Grid uses subscript operator") {
	sudoku::Grid<> g;
	g[{1,2}];
}

TEST_CASE("Grid actually stores cells under coordinates") {
	sudoku::Grid<3, int> g;
	g[{1,2}] = 42;
	int v = g[{1,2}];
	REQUIRE(v == 42);
}

TEST_CASE("Grid subscript operator throws on invalid coord") {
	sudoku::Grid<3, int> g;
	sudoku::Coord zero{0,0};
	CHECK_THROWS_AS(g[zero], sudoku::InvalidCoord);
	sudoku::Coord one{1,1};
	CHECK_NOTHROW(g[one]);
}

TEST_CASE("Grid correctly retains values while copying") {
	sudoku::Grid<3, int> source;
	for (int r = 1; r <= 9; r++) {
		for (int c = 1; c <= 9; c++) {
			source[{r,c}] = 10*r + c;
		}
	}
	sudoku::Grid<3, int> dest{source};
	sudoku::Grid<3, int> dest2{};
	dest2 = source;
	for (int r = 1; r <= 9; r++) {
		for (int c = 1; c <= 9; c++) {
			auto v = dest[{r,c}];
			auto v2 = dest2[{r,c}];
			REQUIRE(v == 10*r + c);
			REQUIRE(v2 == 10*r + c);
		}
	}
}

